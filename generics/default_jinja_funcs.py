from datetime import datetime

from generics.decorators import jinja_func


@jinja_func
def get_current_year():
    return datetime.now().year


@jinja_func
def get_languages():
    return (
        ('uk', 'Українська'),
        ('en', 'English'),
        ('ru', 'Русский'),
    )


@jinja_func
def get_current_language(request):
    return request.COOKIES.get('language', 'en')
