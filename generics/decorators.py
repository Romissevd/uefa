__all__ = (
    'jinja_filter',
    'jinja_func',
    'jinja_test',
)


def jinja_func(func):
    func.is_jinja_func = True
    return func


def jinja_filter(func):
    func.is_jinja_filter = True
    return func


def jinja_test(func):
    func.is_jinja_test = True
    return func
