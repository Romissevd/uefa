from typing import Dict, Optional

from core.models import User
from core.services.core_service import CoreService
from user_profile.models import TelegramProfile, TelegramRegistrationRequest, UserProfile


class TelegramVerificationService:

    @classmethod
    def _create_new_user_from_request(
            cls,
            request: TelegramRegistrationRequest,
            user_data: Dict,
    ) -> User:
        username = cls._get_username_for_telegram_user(request)
        user = User.objects.create_user(
            first_name=user_data['user_first_name'],
            last_name=user_data['user_last_name'],
            middle_name=user_data.get('user_middle_name', ''),
            password=CoreService.password_generator(),
            username=username,
        )
        return user

    @staticmethod
    def _get_username_for_telegram_user(request: TelegramRegistrationRequest) -> str:
        base_username = request.telegram_username
        if User.objects.filter(username=base_username).exists():
            for i in range(1, 10):
                username = f'{base_username}_{i}'
                if User.objects.filter(username=username).exists():
                    continue
                base_username = username
                break
        return base_username

    @classmethod
    def create_telegram_profile(
            cls,
            request: TelegramRegistrationRequest,
            user_data: Dict,
            user: Optional[User] = None,
    ) -> TelegramProfile:
        if not user:
            user = cls._create_new_user_from_request(request, user_data)
        if not hasattr(user, 'profile'):
            profile = UserProfile.objects.create(user=user)
        else:
            profile = user.profile
        telegram_profile = profile.telegram_profiles.create(
            username=request.telegram_username,
            chat_id=request.chat_id,
            user_id=request.user_id,
        )
        return telegram_profile
