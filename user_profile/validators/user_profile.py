from difflib import SequenceMatcher

import django.core.exceptions
from django.contrib.auth import password_validation
from rest_framework import exceptions


class UserProfileValidator:

    @classmethod
    def validate_username_and_password(cls, username: str, password: str) -> bool:
        cls.validate_similar_username_with_password(username, password)
        cls.validate_new_password(password)
        return True

    @staticmethod
    def validate_similar_username_with_password(username: str, password: str) -> bool:
        if SequenceMatcher(a=password.lower(), b=username.lower()).quick_ratio() > 0.7:
            raise exceptions.ValidationError('The password is too similar to the login')
        return True

    @staticmethod
    def validate_new_password(password: str) -> bool:
        try:
            password_validation.validate_password(password)
        except django.core.exceptions.ValidationError as exc:
            raise exceptions.ValidationError(exc.messages)
        return True
