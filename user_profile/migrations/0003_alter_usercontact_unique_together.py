# Generated by Django 3.2.5 on 2022-07-17 13:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0002_alter_userprofile_avatar'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='usercontact',
            unique_together={('value', 'type_contact')},
        ),
    ]
