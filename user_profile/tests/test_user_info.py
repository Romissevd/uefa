from io import BytesIO

import pytest
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from PIL import Image

from user_profile.models import UserContact, UserProfile

User = get_user_model()


def create_test_image(size=(100, 100), image_mode='RGB', image_format='PNG') -> BytesIO:
    """
    Generate a test image, returning the filename that it was saved as.
    """
    data = BytesIO()
    image = Image.new(image_mode, size)
    image.save(data, image_format)
    data.seek(0)
    return data


@pytest.fixture()
def user_account(db):  # pylint: disable=unused-argument
    def _user(**kwargs):
        user = User.objects.create_user(
            username='test',
            first_name='first_name',
            last_name='last_name',
        )
        profile = UserProfile.objects.create(user=user, date_of_birth='1990-01-15')
        UserContact.objects.bulk_create([
            UserContact(profile=profile, value='+380000000000', type_contact=UserContact.TypeContact.PHONE),
            UserContact(profile=profile, value='test@test.com', type_contact=UserContact.TypeContact.EMAIL),
        ])
        return user
    return _user


@pytest.mark.django_db
def test_success_user_info(api_client, user_account):
    user = user_account()
    client = api_client(user)
    url = reverse('user-info')
    response = client.get(url)
    response_json = response.json()
    assert response.status_code == 200
    assert response_json['username'] == 'test'
    assert response_json['first_name'] == 'first_name'
    assert response_json['last_name'] == 'last_name'
    assert response_json['middle_name'] == ''

    profile = response_json['profile']
    assert profile['date_of_birth'] == '1990-01-15'
    assert profile['avatar'] is None

    contacts = profile['contacts']
    assert len(contacts) == 2


@pytest.mark.django_db
def test_change_user_info(api_client, user_account):
    user = user_account()
    assert user.username == 'test'
    assert user.first_name == 'first_name'
    assert user.last_name == 'last_name'
    assert user.middle_name == ''

    client = api_client(user)
    url = reverse('user-info')
    data = {
        'username': 'new_test',
        'first_name': 'new_first_name',
        'last_name': 'new_last_name',
        'middle_name': 'new_middle_name',
        'profile': {
            'date_of_birth': '1995-12-31',
        },
    }
    response = client.patch(url, data=data)
    response_json = response.json()
    assert response.status_code == 200
    assert response_json['username'] == 'test'
    assert response_json['first_name'] == 'new_first_name'
    assert response_json['last_name'] == 'new_last_name'
    assert response_json['middle_name'] == 'new_middle_name'

    profile = response_json['profile']
    assert profile['date_of_birth'] == '1995-12-31'


@pytest.mark.django_db
def test_upload_avatar(api_client, user_account):
    user = user_account()
    profile = user.profile
    assert bool(profile.avatar) is False

    avatar = create_test_image()
    uploaded_file = SimpleUploadedFile('avatar.png', avatar.getvalue(), content_type='multipart/form-data')
    client = api_client(user)

    url = reverse('upload-avatar')
    response = client.post(url, data={'avatar': uploaded_file}, format='multipart', content_type=None)
    response_json = response.json()
    assert response.status_code == 201
    assert response_json['status'] == 'success'
    profile.refresh_from_db()
    assert bool(profile.avatar) is True
