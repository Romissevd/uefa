import pytest
from django.urls import reverse

from core.models import User


@pytest.fixture()
def user_account(db):  # pylint: disable=unused-argument
    def _user(**kwargs):
        user = User.objects.create_user(
            username='test_name',
            first_name='first_name',
            last_name='last_name',
            password='1234TEST',
        )
        return user
    return _user


@pytest.mark.django_db
def test_success_change_password(api_client, user_account):
    user = user_account()
    client = api_client(user)
    url = reverse('change-password')
    data = {
        'old_password': '1234TEST',
        'new_password': 'New123PassTest',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 201
    assert response_json['status'] == 'success'


@pytest.mark.django_db
def test_change_password_with_wrong_old_password(api_client, user_account):
    user = user_account()
    client = api_client(user)
    url = reverse('change-password')
    data = {
        'old_password': 'WrongPassword',
        'new_password': 'New123PassTest',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 400
    assert response_json['old_password'] == ['The old password is incorrect']


@pytest.mark.django_db
def test_change_password_with_new_password_equal_username(api_client, user_account):
    user = user_account()
    client = api_client(user)
    url = reverse('change-password')
    data = {
        'old_password': '1234TEST',
        'new_password': 'test_name',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 400
    assert response_json['new_password'] == ['The password is too similar to the login']


@pytest.mark.django_db
def test_change_password_with_wrong_new_password(api_client, user_account):
    user = user_account()
    client = api_client(user)
    url = reverse('change-password')
    data = {
        'old_password': '1234TEST',
        'new_password': 'test',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 400
    assert response_json['new_password'] == [
        'This password is too short. It must contain at least 8 characters.',
        'This password is too common.',
    ]
