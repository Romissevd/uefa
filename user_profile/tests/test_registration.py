import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse

User = get_user_model()


@pytest.mark.django_db
def test_success_registration(api_client):
    client = api_client()
    url = reverse('registration')
    data = {
        'username': 'test',
        'password': '123TestPass876',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'middle_name': 'middle_name',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 201
    assert response_json['status'] == 'success'
    user = User.objects.get(username='test')
    assert user.first_name == 'first_name'
    assert user.last_name == 'last_name'
    assert user.middle_name == 'middle_name'
    assert user.check_password('123TestPass876') is True
    assert user.type_user == User.TypeUser.FAN
    assert hasattr(user, 'profile') is True


@pytest.mark.django_db
def test_failed_registration_if_exists_user(api_client):
    User.objects.create_user(
        username='test',
        first_name='first_name',
        last_name='last_name',
    )
    client = api_client()
    url = reverse('registration')
    data = {
        'username': 'test',
        'password': '123TestPass876',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'middle_name': 'middle_name',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 400
    assert response_json['username'] == ['A user with that username already exists.']


@pytest.mark.django_db
def test_failed_registration_password_equal_username(api_client):
    client = api_client()
    url = reverse('registration')
    data = {
        'username': '123TestPass876',
        'password': '123TestPass876',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'middle_name': 'middle_name',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 400
    assert response_json['non_field_errors'] == ['The password is too similar to the login']


@pytest.mark.django_db
def test_failed_registration_password_only_number(api_client):
    client = api_client()
    url = reverse('registration')
    data = {
        'username': 'test',
        'password': '12412342113',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'middle_name': 'middle_name',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 400
    assert response_json['password'] == ['This password is entirely numeric.']


@pytest.mark.django_db
def test_failed_registration_common_password(api_client):
    client = api_client()
    url = reverse('registration')
    data = {
        'username': 'test',
        'password': '1234qwer',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'middle_name': 'middle_name',
    }
    response = client.post(url, data=data)
    response_json = response.json()
    assert response.status_code == 400
    assert response_json['password'] == ['This password is too common.']
