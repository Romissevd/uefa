from django.contrib.auth import get_user_model
from django.db import models

from mixins.model_mixin import ContactABC, DatetimeABC

User = get_user_model()


class UserProfile(models.Model):
    """
    Additional information about the user
    """

    class Language(models.TextChoices):
        RU = 'ru', 'Russian'
        UK = 'uk', 'Ukrainian'
        EN = 'en', 'English'

    user = models.OneToOneField(User, models.CASCADE, related_name='profile')
    avatar = models.ImageField(upload_to='avatars/%Y/%m/%d', null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    language = models.CharField(
        max_length=10, choices=Language.choices, default=Language.UK, help_text='Language code format ISO 639-1',
    )


class UserContact(ContactABC):
    """
    User contacts
    """
    profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='contacts')

    class Meta:
        unique_together = ('value', 'type_contact')


class TelegramRegistrationRequest(DatetimeABC):

    class RequestStatus(models.IntegerChoices):
        IN_PROCESS = (1, 'In process')
        APPROVED = (2, 'Approved')
        REJECTED = (3, 'Rejected')

    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    middle_name = models.CharField(max_length=150, blank=True)
    telegram_username = models.CharField(max_length=150)
    user_id = models.BigIntegerField()
    chat_id = models.BigIntegerField()
    status = models.IntegerField(choices=RequestStatus.choices, default=RequestStatus.IN_PROCESS)
    comment = models.TextField(default='')

    @property
    def get_name(self) -> str:
        if self.first_name or self.last_name:
            return f'{self.last_name} {self.first_name}'.strip()
        return f'{self.telegram_username}'


class TelegramProfile(DatetimeABC):
    username = models.CharField(max_length=150, unique=True)
    user_id = models.BigIntegerField()
    chat_id = models.BigIntegerField()
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='telegram_profiles')
