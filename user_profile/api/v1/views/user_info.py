from django.contrib.auth import get_user_model
from drf_spectacular.utils import extend_schema
from rest_framework import exceptions, generics, mixins, parsers, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from user_profile.api.v1.serializers.user_info import (
    UploadUserAvatarSerializer, UserContactSerializer, UserInfoSerializer,
)
from user_profile.models import UserContact

User = get_user_model()


class UserInfoView(generics.RetrieveAPIView, generics.UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserInfoSerializer

    def get_object(self):
        return self.request.user


class UserContactViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    GenericViewSet,
):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = UserContact.objects.all()
    serializer_class = UserContactSerializer

    def get_queryset(self):
        return self.queryset.filter(profile__user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(
            profile=self.request.user.profile,
        )


class UploadAvatarView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    parser_classes = (parsers.FormParser, parsers.MultiPartParser)
    serializer_class = UploadUserAvatarSerializer

    @extend_schema(request=UploadUserAvatarSerializer)
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        if not hasattr(user, 'profile'):
            raise exceptions.ValidationError('User does not have profile')
        profile = user.profile
        profile.avatar.delete()
        profile.avatar = serializer.validated_data['avatar']
        profile.save(update_fields=['avatar'])
        return Response({'status': 'success'}, status=201)
