from django_filters import rest_framework as filters
from rest_framework import mixins, permissions
from rest_framework.viewsets import GenericViewSet

from mixins.pagination_mixin import StandardResultsSetPagination
from mixins.permission_mixin import IsManagerUserPermission
from user_profile.api.v1.filters.verification import TelegramRegistrationRequestFilter
from user_profile.api.v1.serializers.verification import TelegramRegistrationRequestSerializer
from user_profile.models import TelegramRegistrationRequest


class TelegramRequestVerificationViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet,
):
    permission_classes = (permissions.IsAuthenticated, IsManagerUserPermission)
    queryset = TelegramRegistrationRequest.objects.all()
    serializer_class = TelegramRegistrationRequestSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = TelegramRegistrationRequestFilter
    pagination_class = StandardResultsSetPagination
