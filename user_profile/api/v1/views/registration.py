from drf_spectacular.utils import extend_schema
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import User
from user_profile.api.v1.serializers.registration import RegistrationSerializer
from user_profile.models import UserProfile


class RegistrationView(APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = RegistrationSerializer

    @extend_schema(request=RegistrationSerializer)
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.create_user(
            **serializer.validated_data
        )
        UserProfile.objects.create(user=user)
        return Response({'status': 'success'}, status=201)
