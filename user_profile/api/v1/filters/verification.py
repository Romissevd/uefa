from django.db.models import F, Value
from django.db.models.functions import Concat
from django_filters import rest_framework as filters

from user_profile.models import TelegramRegistrationRequest


class TelegramRegistrationRequestFilter(filters.FilterSet):
    created_at = filters.DateFilter()
    created_at_from = filters.DateFilter(field_name='created_at', lookup_expr='gte')
    created_at_to = filters.DateFilter(field_name='created_at', lookup_expr='lte')
    status = filters.ChoiceFilter(choices=TelegramRegistrationRequest.RequestStatus.choices)
    telegram_username = filters.CharFilter(lookup_expr='icontains')
    full_name_user = filters.CharFilter(method='full_name_user_filter')

    @staticmethod
    def full_name_user_filter(queryset, name, value):
        queryset = queryset.alias(
            full_name_user=Concat(
                F('last_name'), Value(' '), F('first_name'), Value(' '), F('middle_name'),
            ),
        )
        return queryset.filter(full_name_user__icontains=value)

    ordering = filters.OrderingFilter(
        fields=(
            ('status', 'status'),
            ('created_at', 'created_at'),
            ('telegram_username', 'telegram_username'),
        ),
    )
