from django.urls import path
from rest_framework import routers

from user_profile.api.v1.views.change_password import ChangePasswordView
from user_profile.api.v1.views.registration import RegistrationView
from user_profile.api.v1.views.user_info import UploadAvatarView, UserContactViewSet, UserInfoView
from user_profile.api.v1.views.verification import TelegramRequestVerificationViewSet

router = routers.SimpleRouter()
router.register('me/contact', UserContactViewSet, basename='user-contact')
router.register('telegram/verification', TelegramRequestVerificationViewSet, basename='telegram-request-verification')

urlpatterns = [
    path('me/', UserInfoView.as_view(), name='user-info'),
    path('me/upload_avatar/', UploadAvatarView.as_view(), name='upload-avatar'),
    path('registration/', RegistrationView.as_view(), name='registration'),
    path('change_password/', ChangePasswordView.as_view(), name='change-password'),
]

urlpatterns += router.urls
