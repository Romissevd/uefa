from rest_framework import serializers

from core.models import User
from user_profile.validators.user_profile import UserProfileValidator


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=20, write_only=True)

    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'middle_name', 'password',
        )

    @staticmethod
    def validate_password(value: str) -> str:
        UserProfileValidator.validate_new_password(value)
        return value

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        UserProfileValidator.validate_similar_username_with_password(username, password)
        return super().validate(attrs)
