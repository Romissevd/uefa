from rest_framework import serializers

from core.models import User
from user_profile.models import UserContact, UserProfile


class UserContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserContact
        fields = (
            'id', 'value', 'type_contact',
        )


class UserProfileSerializer(serializers.ModelSerializer):
    contacts = UserContactSerializer(many=True)

    class Meta:
        model = UserProfile
        fields = (
            'avatar', 'date_of_birth', 'contacts',
        )
        read_only_fields = ('avatar', 'contacts')


class UserInfoSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'middle_name', 'profile',
        )
        read_only_fields = ('username',)

    def update(self, instance, validated_data):
        profile = validated_data.pop('profile', {})
        for attr, value in profile.items():
            setattr(instance.profile, attr, value)
        instance.profile.save()
        return super().update(instance, validated_data)


class UploadUserAvatarSerializer(serializers.Serializer):
    avatar = serializers.ImageField()
