from rest_framework import exceptions, serializers

from core.models import User
from user_profile.models import TelegramRegistrationRequest
from user_profile.services.verification import TelegramVerificationService


class TelegramRegistrationRequestSerializer(serializers.ModelSerializer):
    user_first_name = serializers.CharField(max_length=150, required=False, write_only=True)
    user_last_name = serializers.CharField(max_length=150, required=False, write_only=True)
    user_middle_name = serializers.CharField(max_length=150, required=False, allow_blank=True, write_only=True)
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.filter(profile__isnull=False), required=False, allow_null=True, write_only=True,
    )

    class Meta:
        model = TelegramRegistrationRequest
        fields = (
            'id', 'first_name', 'last_name', 'telegram_username', 'status', 'comment', 'user_first_name',
            'user_last_name', 'user_middle_name', 'user',
        )
        read_only_fields = ('id', 'first_name', 'last_name', 'telegram_username')

    def validate(self, attrs):
        instance = self.instance
        status = attrs.get('status')
        comment = attrs.get('comment')
        user = attrs.get('user')
        user_first_name = attrs.get('user_first_name')
        user_last_name = attrs.get('user_last_name')
        if instance and instance.status != TelegramRegistrationRequest.RequestStatus.IN_PROCESS:
            raise exceptions.ValidationError('This request cannot be changed')
        if status == TelegramRegistrationRequest.RequestStatus.REJECTED and not comment:
            raise exceptions.ValidationError({'comment': 'Comment is required'})
        if (
                status == TelegramRegistrationRequest.RequestStatus.APPROVED and
                (
                    not user and
                    not (user_first_name and user_last_name)
                )
        ):
            raise exceptions.ValidationError('Need selected user or add user_first_name and user_last_name')
        return super().validate(attrs)

    def update(self, instance, validated_data):
        status = validated_data.get('status')
        instance = super().update(instance, validated_data)
        if status:
            if status == TelegramRegistrationRequest.RequestStatus.REJECTED:
                from telegram_bot.tasks import send_message_to_telegram_about_rejected_request
                send_message_to_telegram_about_rejected_request.s(instance).apply_async()
            elif status == TelegramRegistrationRequest.RequestStatus.APPROVED:
                from telegram_bot.tasks import send_message_to_telegram_about_approved_request
                telegram_profile = TelegramVerificationService.create_telegram_profile(instance, validated_data)
                send_message_to_telegram_about_approved_request.s(telegram_profile).apply_async()
        return instance
