from rest_framework import exceptions, serializers

from user_profile.validators.user_profile import UserProfileValidator


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(write_only=True, max_length=20)
    new_password = serializers.CharField(write_only=True, max_length=20)

    def validate_old_password(self, value):
        request = self.context['request']
        user = request.user
        if not user.check_password(value):
            raise exceptions.ValidationError('The old password is incorrect')
        return value

    def validate_new_password(self, value):
        request = self.context['request']
        user = request.user
        UserProfileValidator.validate_username_and_password(user.username, value)
        return value

    def validate(self, attrs):
        old_password = attrs['old_password']
        new_password = attrs['new_password']
        if old_password == new_password:
            raise exceptions.ValidationError('Old and new passwords are the same')
        return super().validate(attrs)
