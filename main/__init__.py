from __future__ import absolute_import, unicode_literals

from main.celery import celery_app


__all__ = ['celery_app']
