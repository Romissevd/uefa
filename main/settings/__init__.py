from main.settings.environment import env  # noqa F401, F403
from main.settings.django import *  # noqa F401, F403
from main.settings.contrib import *  # noqa F401, F403
from main.settings.project import *  # noqa F401, F403

environment = env.str('ENVIRONMENT', 'production')
if environment == 'production':
    from main.settings.production_settings import *  # noqa F401, F403
else:
    from main.settings.dev_settings import *  # noqa F401, F403
