import os.path
from pathlib import Path

import environ

env_dir = Path(__file__).resolve().parents[2]
env = environ.Env()
environ.Env.read_env(os.path.join(env_dir, '.env'))
