from main.settings.environment import env

ENABLE_UPLOAD_TEAM_LOGO = env.bool('ENABLE_UPLOAD_TEAM_LOGO', False)

ADMIN_AUTH_HEADER_KEY = 'Admin-Auth-Token'
ADMIN_AUTH_TOKEN = env.str('ADMIN_AUTH_TOKEN', '46U2ZKoMLRDRZPpO89rBM5O92wr5Ng63')
