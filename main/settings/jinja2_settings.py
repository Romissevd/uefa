from importlib import import_module
from itertools import chain

from django.apps import apps
from django.conf import settings
from django.templatetags.static import static
from django.utils.module_loading import module_has_submodule
from django.utils.translation import gettext, ngettext
from jinja2 import Environment
from jinja2.ext import Extension, i18n

from generics import default_jinja_funcs


def initialize_jinja_funcs(env: Environment):
    for module in chain((default_jinja_funcs,), get_app_modules()):
        for func_name in dir(module):
            jinja_func = getattr(module, func_name)
            if (
                isinstance(jinja_func, type) and
                issubclass(jinja_func, Extension) and
                jinja_func is not Exception
            ):
                env.add_extension(jinja_func)
            if getattr(jinja_func, 'is_jinja_func', False):
                env.globals[func_name] = jinja_func
            elif getattr(jinja_func, 'is_jinja_filter', False):
                env.filters[func_name] = jinja_func
            elif getattr(jinja_func, 'is_jinja_test', False):
                env.tests[func_name] = jinja_func


def get_app_modules():
    jinja_funcs_file_name = 'jinja_funcs'
    for app_config in apps.get_app_configs():
        try:
            yield import_module(f'{app_config.name}.{jinja_funcs_file_name}')
        except Exception:
            if module_has_submodule(app_config.module, jinja_funcs_file_name):
                raise


def environment(**options):
    env_options = {
        'auto_reload': settings.DEBUG,
        'autoescape': True,
        'extensions': (i18n,),
        # 'loader': FileSystemLoader()
    }
    env_options.update(options)
    env = Environment(**env_options)
    initialize_jinja_funcs(env)
    env.globals.update({
        'static': static,
        'media_url': settings.MEDIA_URL,
        '_': gettext,
        'ngettext': ngettext,
    })
    return env
