MODELTRANSLATION_DEFAULT_LANGUAGE = 'en'
MODELTRANSLATION_LANGUAGES = ('en', 'uk', 'ru')
MODELTRANSLATION_TRANSLATION_FILES = (
    'directory.translation',
    'champions_league.translation',
    'team.translation',
)
MODELTRANSLATION_FALLBACK_LANGUAGES = {
    'default': ('en', 'uk', 'ru'),
    'ru': ('en',),
    'uk': ('en',),
}
