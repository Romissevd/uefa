from main.settings.contrib.celery import *  # noqa F401, F403
from main.settings.contrib.third_party_api import *  # noqa F401, F403
from main.settings.contrib.spectacular import *  # noqa F401, F403
from main.settings.contrib.rest_framework import *  # noqa F401, F403
from main.settings.contrib.logger import *  # noqa F401, F403
from main.settings.contrib.telegram import *  # noqa F401, F403
