SPECTACULAR_SETTINGS = {
    'TITLE': 'UEFA API',
    'DESCRIPTION': 'UEFA betting automation',
    'VERSION': '1.0.0',
    'SCHEMA_PATH_PREFIX': r'/api/v[1-9]',
    'COMPONENT_SPLIT_REQUEST': True
}
