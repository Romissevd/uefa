import logging.config
import os

from django.conf import settings
from django.utils.log import DEFAULT_LOGGING

from generics.exceptions import SettingsError
from main.settings.environment import env

LOG_LEVEL = env.str('LOG_LEVEL', 'debug').upper()
PATH_TO_LOG_FILES = '/var/log/uefa'

if LOG_LEVEL not in logging.getLevelNamesMapping():
    raise SettingsError(
        'LOG LEVEL is wrong. Allowed values: %s. Current value - %s.' % (
            ', '.join(logging.getLevelNamesMapping().keys()),
            LOG_LEVEL,
        ),
    )


if not (os.path.exists(PATH_TO_LOG_FILES) or os.access(PATH_TO_LOG_FILES, os.W_OK)):
    PATH_TO_LOG_FILES = f'{settings.BASE_DIR.parent}/logs'
    if not os.path.exists(PATH_TO_LOG_FILES):
        os.mkdir(PATH_TO_LOG_FILES)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(asctime)-4s - %(levelname)8s: %(name)s.%(funcName)s: %(message)s',
            'datefmt': '%d/%b/%Y %H:%M:%S',
        },
        'simple': {
            'format': '{levelname}: {message}',
            'style': '{',
        },
        'django.server': DEFAULT_LOGGING['formatters']['django.server'],
    },
    'filters': {
        'custom_filter': {
            '()': 'main.log_filters.CustomFilter',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'console': {
            'level': LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(PATH_TO_LOG_FILES, 'django.log'),
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 5,
            'formatter': 'django.server',
            'filters': ['require_debug_false'],
        },
        'file': {
            'level': LOG_LEVEL,
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(PATH_TO_LOG_FILES, 'app.log'),
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 5,
            'filters': ['custom_filter'],
        },
    },
    'loggers': {
        'django.server': {
            'handlers': ['django.server', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        settings.PROJECT: {
            'handlers': ['file', 'console'],
            'level': LOG_LEVEL,
            'propagate': True,
        },
    },
}


logging.config.dictConfig(LOGGING)
