from main.settings.environment import env

CHAMPIONS_LEAGUE_URL = env.str('CHAMPIONS_LEAGUE_URL', '')
CHAMPIONS_LEAGUE_API_TOKEN = env.str('CHAMPIONS_LEAGUE_API_TOKEN', '')
