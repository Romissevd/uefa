from main.settings.contrib import *  # noqa F401, F403
from main.settings.django import *  # noqa F401, F403
from main.settings.environment import env  # noqa F401, F403
from main.settings.project import *  # noqa F401, F40

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env.str('POSTGRES_DB', 'sport-db'),
        'USER': env.str('POSTGRES_USER', 'postgres'),
        'PASSWORD': env.str('POSTGRES_PASSWORD', 'test'),
        'HOST': env.str('POSTGRES_HOST', 'postgres'),
        'PORT': env.str('POSTGRES_PORT', '5432'),
    },
}
