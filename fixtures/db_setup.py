import pytest
from django.db import connection


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    """Test session DB setup."""
    with django_db_blocker.unblock():
        with connection.cursor() as cursor:
            cursor.execute('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
            # cursor.execute('CREATE EXTENSION IF NOT EXISTS pg_trgm;')
