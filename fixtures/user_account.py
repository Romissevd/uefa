import pytest
from model_bakery import baker


@pytest.fixture()
def user_account(db):  # pylint: disable=unused-argument
    def _user(**kwargs):
        user_permissions = kwargs.get('user_permissions', None)
        groups = kwargs.get('groups', None)
        user = baker.make('core.User', **kwargs)
        if user_permissions:
            user.user_permissions.set(user_permissions)
        if groups:
            user.groups.set(groups)
        return user

    return _user


# @pytest.fixture()
# def user_profile(user_account):
#     def _user_profile(**kwargs):
#         user = kwargs.pop('user', None) or user_account()
#         user_profile = baker.make(
#             'core.UserProfile',
#             user=user,
#             **kwargs)
#         return user_profile
#
#     return _user_profile
