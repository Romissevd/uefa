import logging

from django.conf import settings

from monitoring.constants import ALLOWED_LOG_LEVELS

logger = logging.getLogger(settings.PROJECT).getChild(__name__)


def create_log_records(message: str, level: str) -> None:
    level = level.upper()

    if level == 'ALL':
        for level_name in ALLOWED_LOG_LEVELS:
            if level_name == 'ALL':
                continue
            create_log_records(message, level_name)
        return

    message = f'Called level name: {level}. {message}'

    match level:
        case 'DEBUG':
            logger.debug(message)
        case 'INFO':
            logger.info(message)
        case 'WARN' | 'WARNING':
            logger.warning(message)
        case 'CRITICAL' | 'FATAL':
            logger.critical(message)
        case 'ERROR':
            logger.error(message)
        case 'NOTSET':
            logger.log(logging.NOTSET, message)
        case _:
            logger.error(
                'Log level - %s is not allowed. Allowed: %s.' % (level, ', '.join(ALLOWED_LOG_LEVELS))
            )
