from django.urls import path

from monitoring.views.logs import LogsView

urlpatterns = [
    path('logs/', LogsView.as_view(), name='logs-view'),
]
