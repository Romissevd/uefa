from rest_framework import serializers

from monitoring.constants import ALLOWED_LOG_LEVELS


class LogsSerializer(serializers.Serializer):
    log_message = serializers.CharField()
    level = serializers.ChoiceField(choices=ALLOWED_LOG_LEVELS)
