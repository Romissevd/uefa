import logging

ALLOWED_LOG_LEVELS = list(logging.getLevelNamesMapping().keys()) + ['ALL']
