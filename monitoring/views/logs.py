import logging
from http import HTTPStatus

from django.conf import settings
from django.http import HttpResponse, HttpResponseForbidden
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from monitoring.serializers.logs import LogsSerializer
from monitoring.utils import create_log_records

logger = logging.getLogger(settings.PROJECT).getChild(__name__)


class LogsView(APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = LogsSerializer

    def dispatch(self, request, *args, **kwargs):
        if settings.ADMIN_AUTH_HEADER_KEY not in request.headers:
            logger.error('Request without required header key.')
            return HttpResponse(b'{"status": "error"}', status=HTTPStatus.UNAUTHORIZED)
        if request.headers[settings.ADMIN_AUTH_HEADER_KEY] != settings.ADMIN_AUTH_TOKEN:
            logger.error('Required header value is wrong.')
            return HttpResponseForbidden(b'{"status": "error"}')
        return super().dispatch(request, *args, **kwargs)

    @extend_schema(
        parameters=[
            LogsSerializer,
            OpenApiParameter(settings.ADMIN_AUTH_HEADER_KEY, OpenApiTypes.STR, OpenApiParameter.HEADER),
        ],
    )
    def get(self, request, *args, **kwargs):
        self.generate_logs(request.query_params)
        return Response({'status': 'ok'})

    @extend_schema(
        parameters=[
            OpenApiParameter(settings.ADMIN_AUTH_HEADER_KEY, OpenApiTypes.STR, OpenApiParameter.HEADER),
        ],
        request=LogsSerializer,
    )
    def post(self, request, *args, **kwargs):
        self.generate_logs(request.data)
        return Response({'status': 'ok'})

    def generate_logs(self, request_data):
        serializer = self.serializer_class(data=request_data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        create_log_records(data['log_message'], data['level'])
