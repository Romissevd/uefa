from modeltranslation.translator import TranslationOptions, translator

from directory.models import Country


class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)


translator.register(Country, CountryTranslationOptions)
