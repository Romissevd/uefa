from rest_framework import permissions, viewsets

from directory.api.v1.serializers.general import CountrySerializer
from directory.models import Country


class CountryViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CountrySerializer
    queryset = Country.objects.all()
