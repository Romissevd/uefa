from rest_framework.routers import SimpleRouter

from directory.api.v1.views.general import CountryViewSet

router = SimpleRouter()
router.register('country', CountryViewSet)

urlpatterns = []

urlpatterns += router.urls
