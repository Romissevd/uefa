from mixins.model_mixin import NameABC


class Country(NameABC):
    """
    Country model
    """

    def __str__(self):
        return self.name
