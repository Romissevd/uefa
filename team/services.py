import logging
from typing import Dict

from django.conf import settings

from team.models import Team
from team.tasks import upload_logo

logger = logging.getLogger(settings.PROJECT).getChild(__name__)


class TeamServices:

    @staticmethod
    def get_or_create_team(team_info: Dict) -> Team:
        """
        Get or create team
        Args:
            team_info (dict): team info from API service
        Returns:
            Team: object
        Note:
            The search for a command in the database is carried out by the identifier from the api service.
            If there is no match, a new team is created.
            A task is also launched to download the team logo.
        """
        team_api_id = team_info['id']
        team_name = team_info.get('name')
        team, _ = Team.objects.get_or_create(
            api_id=team_api_id,
            defaults={
                'name': team_info.get('name'),
                'short_name': team_info.get('shortName', '') or '',
                'founded': team_info.get('founded'),
                'address': team_info.get('address') or '',
                'web_site': team_info.get('website') or '',
                'api_id': team_api_id,
                'logo': None,
            },
        )
        logo_url = team_info.get('crest')
        if logo_url and not team.logo and settings.ENABLE_UPLOAD_TEAM_LOGO:
            upload_logo.s(team_api_id, logo_url).apply_async()
        else:
            logger.info(f'The team {team_name} (ID - {team_api_id}) does not have a logo image')
        return team
