from django.contrib import admin

from team.models import Team


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name', 'name_en', 'name_ru', 'name_uk')
