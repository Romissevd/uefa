# Generated by Django 3.2.5 on 2023-09-10 16:37

import django.core.validators
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('directory', '0003_auto_20230910_1547'),
        ('team', '0004_auto_20230910_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='directory.country'),
        ),
        migrations.AlterField(
            model_name='team',
            name='address',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='team',
            name='founded',
            field=models.SmallIntegerField(
                blank=True, null=True,
                validators=[
                    django.core.validators.MaxValueValidator(2050),
                    django.core.validators.MinValueValidator(1850),
                ],
            ),
        ),
        migrations.AlterField(
            model_name='team',
            name='logo',
            field=models.ImageField(blank=True, null=True, upload_to='logo'),
        ),
        migrations.AlterField(
            model_name='team',
            name='short_name',
            field=models.CharField(blank=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='team',
            name='web_site',
            field=models.URLField(blank=True, default=''),
        ),
    ]
