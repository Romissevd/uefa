from modeltranslation.translator import TranslationOptions, translator

from team.models import Team


class TeamTranslationOptions(TranslationOptions):
    fields = ('name',)
    fallback_languages = {'default': ('en', 'uk', 'ru'), 'ru': ('en',), 'uk': ('en',)}


translator.register(Team, TeamTranslationOptions)
