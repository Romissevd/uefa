from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from mixins.model_mixin import ContactABC, NameABC


class Team(NameABC):
    """
    Team model
    """
    short_name = models.CharField(max_length=100, default='', blank=True)
    logo = models.ImageField(upload_to='logo', null=True, blank=True)
    country = models.ForeignKey('directory.Country', on_delete=models.PROTECT, null=True, blank=True)
    founded = models.SmallIntegerField(
        null=True, blank=True, validators=[MaxValueValidator(2050), MinValueValidator(1850)],
    )
    web_site = models.URLField(default='', blank=True)
    address = models.CharField(max_length=200, default='', blank=True)
    api_id = models.SmallIntegerField()


class TeamContact(ContactABC):
    """
    Team contact model
    """
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='contacts')

    class Meta:
        unique_together = (
            ('team', 'value', 'type_contact'),
        )
