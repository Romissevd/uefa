import logging
from io import BytesIO
from uuid import uuid4

import requests
from django.core.files.images import ImageFile

from main import celery_app
from team.models import Team

logger = logging.getLogger('app').getChild(__name__)


@celery_app.task
def upload_logo(team_api_id: int, logo_url: str) -> bool:
    """
    Upload team logo
    Args:
        team_api_id (int): specified ID team in the service api response
        logo_url (str): image url
    Returns:
        bool: True if successful upload else False
    """
    response = requests.get(logo_url)
    if response.status_code != 200:
        logger.error(f'Error loading image. Team ID - {team_api_id}. Status code - {response.status_code}')
        return False
    file_extension = logo_url.split('.')[-1]
    team = Team.objects.get(api_id=team_api_id)
    team.logo = ImageFile(BytesIO(response.content), name=f'{uuid4()}.{file_extension}')
    team.save()
    return True


@celery_app.task
def parse_teams() -> None:
    """
    Getting all available teams from the API service and writing information about them to the database
    Note:
        The function can be executed for a long time due to the limited number of requests to the API service
    """
    from champions_league.services.api_service import ChampionsLeagueAPIService
    from team.services import TeamServices
    limit = 500
    step = 1
    parse = True
    while parse:
        offset = limit * (step - 1)
        url = f'teams/?limit={limit}&offset={offset}'
        response_json = ChampionsLeagueAPIService.get_json_info(url)
        result = response_json.get('teams')
        if not response_json or not result:
            break
        for team_info in result:
            TeamServices.get_or_create_team(team_info)
        step += 1
