# Generated by Django 3.2.5 on 2023-09-10 16:23

from django.db import migrations, models


def copy_en_values_in_default_column(apps, schema_editor):
    Stage = apps.get_model('champions_league', 'ChampionsLeagueStage')
    db_alias = schema_editor.connection.alias
    for stage in Stage.objects.using(db_alias).all():
        stage.name = stage.name_en
        stage.save()


class Migration(migrations.Migration):

    dependencies = [
        ('champions_league', '0006_alter_game_options'),
    ]

    operations = [
        migrations.RenameField(
            model_name='championsleaguestage',
            old_name='name_eng',
            new_name='name_en',
        ),
        migrations.AlterField(
            model_name='championsleaguestage',
            name='name_en',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.RemoveField(
            model_name='championsleaguestage',
            name='name_rus',
        ),
        migrations.AddField(
            model_name='championsleaguestage',
            name='name_ru',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='championsleaguestage',
            name='name_uk',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='championsleaguestage',
            name='name',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.RunPython(copy_en_values_in_default_column, reverse_code=migrations.RunPython.noop),
    ]
