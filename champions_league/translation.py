from modeltranslation.translator import TranslationOptions, translator

from champions_league.models import ChampionsLeagueStage


class StageTranslationOptions(TranslationOptions):
    fields = ('name',)


translator.register(ChampionsLeagueStage, StageTranslationOptions)
