from datetime import datetime, timedelta
from urllib.parse import urlencode

import pytest
from django.urls import reverse
from model_bakery import baker

from champions_league.models import Game


@pytest.mark.django_db
def test_filter_list_game(api_client):
    baker.make(
        'champions_league.Game',
        start_season='2022-06-01',
        game_date='2022-06-01',
        status=Game.GameStatus.FINISHED,
    )
    baker.make(
        'champions_league.Game',
        start_season='2022-06-01',
        game_date='2021-06-01',
        status=Game.GameStatus.SCHEDULED,
    )
    baker.make(
        'champions_league.Game',
        start_season='2021-06-01',
        game_date='2022-12-01',
        status=Game.GameStatus.SCHEDULED,
    )
    client = api_client()
    url = reverse('games-list')

    response = client.get(url)
    response_json = response.json()

    assert response.status_code == 200
    assert len(response_json['results']) == 3

    query_params = urlencode({'start_season': 2022})
    response = client.get(url + '?' + query_params)
    response_json = response.json()
    assert response.status_code == 200
    assert len(response_json['results']) == 2

    query_params = urlencode({'game_date': '2022-06-01'})
    response = client.get(url + '?' + query_params)
    response_json = response.json()
    assert response.status_code == 200
    assert len(response_json['results']) == 1

    query_params = urlencode({'game_date_from': '2022-01-01'})
    response = client.get(url + '?' + query_params)
    response_json = response.json()
    assert response.status_code == 200
    assert len(response_json['results']) == 2

    query_params = urlencode({'game_date_to': '2022-01-01'})
    response = client.get(url + '?' + query_params)
    response_json = response.json()
    assert response.status_code == 200
    assert len(response_json['results']) == 1

    query_params = urlencode({'status': Game.GameStatus.SCHEDULED})
    response = client.get(url + '?' + query_params)
    response_json = response.json()
    assert response.status_code == 200
    assert len(response_json['results']) == 2


@pytest.mark.django_db
def test_next_games(api_client):
    team_1 = baker.make('team.Team')
    team_2 = baker.make('team.Team')
    datetime_now = datetime.now()
    baker.make(
        'champions_league.Game',
        game_date=datetime_now - timedelta(days=1),
        away_team=team_1,
        home_team=team_2,
        status=Game.GameStatus.SCHEDULED,
    )
    baker.make(
        'champions_league.Game',
        game_date=datetime_now + timedelta(days=1),
        away_team=team_1,
        home_team=team_2,
        status=Game.GameStatus.SCHEDULED,
    )
    client = api_client()
    url = reverse('next-games-list')

    response = client.get(url)
    response_json = response.json()
    assert response.status_code == 200
    assert len(response_json) == 1
