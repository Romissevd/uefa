import logging
import re
import time
from typing import Tuple, Union

import requests
from django.conf import settings

from team.models import Team

logger = logging.getLogger(settings.PROJECT).getChild(__name__)


class ChampionsLeagueAPIService:
    headers = {'X-Auth-Token': settings.CHAMPIONS_LEAGUE_API_TOKEN}
    game_status_dict = {
        'SCHEDULED': 1,
        'FINISHED': 2,
    }

    @classmethod
    def get_json_info(cls, url: str) -> dict:
        """
        Gets information from the API service
        Args:
            url (str): part of the url that will be added to the base url of the request
        Returns:
            dict: data from the API service or an empty dictionary in case of a response from the server with
            a code other than 200
        Note:
            If a response status with code 429 was received while requesting, the function will try to get
            from the response the timeout until the next request and will sleep for the specified time
            if the time does not exceed 60 seconds
        """
        request_url = f'{settings.CHAMPIONS_LEAGUE_URL}{url}'
        response = requests.get(request_url, headers=cls.headers)
        response_json = response.json()
        if response.status_code == 429:
            message = response_json.get('message', '')
            wait_time = re.search(r'\d+(?= seconds)', message).group()
            if wait_time and wait_time.isdigit() and int(wait_time) <= 60:
                time.sleep(int(wait_time) + 1)
                cls.get_json_info(request_url)
            else:
                response_json = {}
        elif response.status_code != 200:
            logger.error(f'API request returned an error. Status code - {response.status_code}')
            response_json = {}
        return response_json

    @staticmethod
    def get_teams_by_api_info(team_info: dict) -> Union[Tuple[Team, Team], Tuple[None, None]]:
        """
        Gets teams from the database based on the received data from the service
        Args:
            team_info (dict): data from the API service
        Returns:
            tuple: Team objects (Home team, away team) or (None, None)
        """
        home_team_api_id = team_info['homeTeam']['id']
        away_team_api_id = team_info['awayTeam']['id']
        message = ''
        if not (home_team_api_id and away_team_api_id):
            return None, None
        try:
            home_team = Team.objects.get(api_id=home_team_api_id)
        except Team.DoesNotExist:
            home_team = None
            message += f' Home team ID - {home_team_api_id}'
        try:
            away_team = Team.objects.get(api_id=away_team_api_id)
        except Team.DoesNotExist:
            away_team = None
            message += f' Away team ID - {away_team_api_id}'
        if not (home_team and away_team):
            logger.error(f'Team not received.{message}')
            return None, None
        return home_team, away_team

    @staticmethod
    def parse_game_score(match_info: dict) -> dict:
        """
        Gets match score information from full match info
        Args:
            match_info (dict): full match information
        Returns:
            dict: match score information
        """
        score = match_info['score']
        game_score = {
            'goals_home': score.get('fullTime', {}).get('home'),
            'goals_away': score.get('fullTime', {}).get('away'),
            'goals_extra_home': score.get('extraTime', {}).get('homeTeam'),
            'goals_extra_away': score.get('extraTime', {}).get('awayTeam'),
            'penalties_home': score.get('penalties', {}).get('homeTeam'),
            'penalties_away': score.get('penalties', {}).get('awayTeam'),
        }
        return game_score

    @classmethod
    def convert_status_game(cls, status: Union[str, int]) -> Union[int, str]:
        if isinstance(status, str):
            return cls.game_status_dict.get(status, 1)
        return list(cls.game_status_dict.keys())[list(cls.game_status_dict.values()).index(status)]
