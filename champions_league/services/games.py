from datetime import date, datetime
from itertools import groupby
from typing import List, Tuple

import pytz
from django.db.models import QuerySet

from champions_league.models import Game
from user_profile.models import UserProfile


class GamesService:

    @classmethod
    def get_next_games(cls) -> QuerySet[Game]:
        games = Game.objects.filter(
            status=Game.GameStatus.SCHEDULED,
            game_date__gte=date.today(),
        )
        games_id = cls._get_game_ids_for_unique_team(games)
        return Game.objects.filter(id__in=games_id).order_by('game_date')

    @classmethod
    def get_next_games_data(
            cls,
            language: UserProfile.Language = UserProfile.Language.EN,
    ) -> List[Tuple[datetime, str]]:
        home_team_name = f'home_team__name_{language}'
        away_team_name = f'away_team__name_{language}'
        next_games = cls.get_next_games()
        result = []
        for obj in next_games.values(
                'game_date', 'home_team__name_en', 'away_team__name_en', home_team_name, away_team_name,
        ):
            home_team = obj[home_team_name] or obj['home_team__name_en']
            away_team = obj[away_team_name] or obj['away_team__name_en']
            result.append((obj['game_date'], f'{home_team} - {away_team}'))
        return result

    @classmethod
    def get_next_games_text(cls, language: UserProfile.Language = UserProfile.Language.EN) -> str:
        next_games = cls.get_next_games_data(language)
        result = cls.get_text_games_group_by_game_date(next_games)
        return result

    @classmethod
    def get_last_games_data(
            cls,
            language: UserProfile.Language = UserProfile.Language.EN,
    ) -> List[Tuple[datetime, str]]:
        home_team_name = f'home_team__name_{language}'
        away_team_name = f'away_team__name_{language}'
        next_games = cls.get_last_games()
        result = []
        for obj in next_games.values(
                'game_date', 'home_team__name_en', 'away_team__name_en', 'goals_home', 'goals_away',
                home_team_name, away_team_name,
        ):
            home_team = obj[home_team_name] or obj['home_team__name_en']
            away_team = obj[away_team_name] or obj['away_team__name_en']
            result.append((obj['game_date'], f'{home_team} {obj["goals_home"]} - {obj["goals_away"]} {away_team}'))
        return result

    @classmethod
    def get_list_last_games_text(
            cls,
            language: UserProfile.Language = UserProfile.Language.EN,
    ) -> str:
        last_games = cls.get_last_games_data(language)
        result = cls.get_text_games_group_by_game_date(last_games)
        return result

    @classmethod
    def get_last_games(cls) -> QuerySet[Game]:
        games = Game.objects.filter(
            status=Game.GameStatus.FINISHED,
            game_date__lte=date.today(),
        ).order_by('-game_date')
        games_id = cls._get_game_ids_for_unique_team(games)
        return Game.objects.filter(id__in=games_id)

    @staticmethod
    def _get_game_ids_for_unique_team(games: QuerySet[Game]) -> List[int]:
        teams = []
        games_id = []
        current_stage = None
        current_season = None
        for game in games:
            if current_stage is None:
                current_stage = game.stage_id
                current_season = game.start_season
            if (
                    current_season != game.start_season or
                    current_stage != game.stage_id or
                    game.away_team_id in teams or
                    game.home_team_id in teams
            ):
                continue
            teams.append(game.home_team_id)
            teams.append(game.away_team_id)
            games_id.append(game.pk)
        return games_id

    @classmethod
    def get_text_games_group_by_game_date(cls, games: List[Tuple[datetime, str]]) -> str:
        result_str = ''
        for game_datetime, group_games in groupby(games, lambda x: x[0]):
            result_str += cls._get_game_ukrainian_datetime(game_datetime) + '\n'
            for game in group_games:
                result_str += game[1] + '\n'
            result_str += '\n'
        return result_str

    @staticmethod
    def _get_game_ukrainian_datetime(game_date: datetime) -> str:
        ukrainian_game_datetime = game_date.replace(tzinfo=pytz.utc).astimezone(pytz.timezone('Europe/Kiev'))
        ukrainian_game_datetime_str = ukrainian_game_datetime.strftime('%H:%M %d-%m-%Y')
        return ukrainian_game_datetime_str
