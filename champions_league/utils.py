import json
from datetime import timedelta

from django_celery_beat.models import ClockedSchedule, PeriodicTask


def create_scheduling_game_task(game_api_id, game_datetime, delta_hours=3):
    """
    Creates a periodic task to download game results
    :param game_api_id:
    :param game_datetime:
    :param delta_hours:
    :return: bool
    """
    end_game_datetime = game_datetime + timedelta(hours=delta_hours)
    str_end_game_datetime = end_game_datetime.strftime('%Y-%m-%d %H:%M')
    clocked_schedule, _ = ClockedSchedule.objects.get_or_create(clocked_time=end_game_datetime)
    PeriodicTask.objects.get_or_create(
        clocked=clocked_schedule,
        name=f'Download result game - {game_api_id} {str_end_game_datetime}',
        task='champions_league.tasks.download_result_game',
        args=json.dumps([game_api_id]),
        one_off=True,
    )
    return True
