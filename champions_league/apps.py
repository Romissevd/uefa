from django.apps import AppConfig


class ChampionsLeagueConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'champions_league'
