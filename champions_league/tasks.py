import logging
from datetime import datetime

from django.conf import settings
from django.utils import timezone

from champions_league import utils
from champions_league.models import ChampionsLeagueStage, Game
from main import celery_app

logger = logging.getLogger(settings.PROJECT).getChild(__name__)


@celery_app.task
def download_games_info():
    """
    Downloads info for the Champions League games of the current season
    Returns:
        bool: True if successful download and processed games info else False
    """
    from champions_league.services.api_service import ChampionsLeagueAPIService as CLService
    json_info = CLService.get_json_info('competitions/CL/matches')
    matches = json_info.get('matches')
    if not matches:
        logger.error('JSON response does not contain information about games')
        return False
    list_new_games = []
    start_season = matches[0]['season']['startDate']
    exists_games = set(Game.objects.filter(start_season=start_season).values_list('api_id', flat=True))
    for match in matches:
        home_team, away_team = CLService.get_teams_by_api_info(match)
        if not (home_team or away_team):
            continue
        game_status = CLService.convert_status_game(match['status'])
        season = match['season']
        api_id = match['id']
        game_datetime = datetime.strptime(match['utcDate'], '%Y-%m-%dT%H:%M:%SZ')
        stage, _ = ChampionsLeagueStage.objects.get_or_create(name=match.get('stage'))
        game_score = CLService.parse_game_score(match) if game_status == Game.GameStatus.FINISHED else {}
        if api_id not in exists_games:
            list_new_games.append(
                Game(
                    home_team=home_team,
                    away_team=away_team,
                    status=game_status,
                    game_date=game_datetime,
                    stage=stage,
                    group=match.get('group')[-1] if match.get('group') else '',
                    start_season=season.get('startDate'),
                    end_season=season.get('endDate'),
                    api_id=api_id,
                    **game_score,
                ),
            )
            if game_status != Game.GameStatus.FINISHED:
                utils.create_scheduling_game_task(api_id, game_datetime)
        else:
            Game.objects.filter(api_id=api_id).update(
                status=game_status,
                **game_score,
            )
    Game.objects.bulk_create(list_new_games, ignore_conflicts=True)
    return True


@celery_app.task
def download_result_game(game_api_id: int) -> bool:
    """
    Downloads information about a specific Champions League match.
    Args:
        game_api_id (int): specified ID game in the service api response
    Returns:
        bool: True if successful download and processed result game else False
    """
    from champions_league.services.api_service import ChampionsLeagueAPIService as CLService
    match_info = CLService.get_json_info(f'matches/{game_api_id}/')
    if not match_info:
        logger.error(f'JSON response does not contain information about game ID {game_api_id}')
        return False
    home_team, away_team = CLService.get_teams_by_api_info(match_info)
    if not (home_team or away_team):
        return False
    game_status = CLService.convert_status_game(match_info['status'])
    game_datetime = datetime.strptime(match_info['utcDate'], '%Y-%m-%dT%H:%M:%SZ')
    stage, _ = ChampionsLeagueStage.objects.get_or_create(name=match_info.get('stage'))
    season = match_info['season']
    if game_status != Game.GameStatus.FINISHED:
        utils.create_scheduling_game_task(game_api_id, timezone.now(), delta_hours=1)
        logger.info(
            f'The game ID {game_api_id} status is not equal to the value FINISHED. Status is {match_info["status"]}',
        )
        return False
    game_score = CLService.parse_game_score(match_info) if game_status == Game.GameStatus.FINISHED else {}
    Game.objects.update_or_create(
        home_team=home_team,
        away_team=away_team,
        game_date=game_datetime,
        stage=stage,
        group=match_info.get('group')[-1] if match_info.get('group') else '',
        start_season=season.get('startDate'),
        end_season=season.get('endDate'),
        api_id=game_api_id,
        defaults={
            'status': game_status,
            **game_score,
        },
    )
    return True
