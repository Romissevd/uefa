from django.db import models

from mixins.model_mixin import DatetimeABC, NameABC


class ChampionsLeagueStage(NameABC):
    """
    Champions League stage model
    """
    pass


class Game(DatetimeABC):
    """
    Game model
    """
    class GameStatus(models.IntegerChoices):
        SCHEDULED = 1
        FINISHED = 2

    home_team = models.ForeignKey('team.Team', on_delete=models.PROTECT, related_name='home_games')
    away_team = models.ForeignKey('team.Team', on_delete=models.PROTECT, related_name='away_games')
    game_date = models.DateTimeField()
    start_season = models.DateField(null=True)
    end_season = models.DateField(null=True)
    status = models.SmallIntegerField(choices=GameStatus.choices, default=GameStatus.SCHEDULED)
    goals_home = models.SmallIntegerField(null=True)
    goals_away = models.SmallIntegerField(null=True)
    goals_extra_home = models.SmallIntegerField(null=True)
    goals_extra_away = models.SmallIntegerField(null=True)
    penalties_home = models.SmallIntegerField(null=True)
    penalties_away = models.SmallIntegerField(null=True)
    stage = models.ForeignKey(ChampionsLeagueStage, on_delete=models.PROTECT)
    group = models.CharField(max_length=1, default='')
    api_id = models.IntegerField(null=True)

    class Meta:
        unique_together = (
            ('home_team', 'away_team', 'game_date'),
        )
        ordering = ['game_date']
