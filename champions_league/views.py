from django.views import View
from django.views.generic.base import TemplateResponseMixin

from champions_league.services.games import GamesService


class IndexView(TemplateResponseMixin, View):
    http_method_names = [
        'get',
    ]
    template_name = 'champions_league/index.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_context_data(self):
        context = {
            'last_games': GamesService.get_last_games(),
            'next_games': GamesService.get_next_games(),
        }
        return context
