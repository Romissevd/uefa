from django_filters import rest_framework as filters
from rest_framework import mixins, permissions
from rest_framework.viewsets import GenericViewSet

from champions_league.api.v1.filtrers.game import GameFilter
from champions_league.api.v1.serilazers.game import GameSerializer, NextGameSerializer
from champions_league.models import Game
from champions_league.services.games import GamesService
from mixins.pagination_mixin import StandardResultsSetPagination


class GameViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, GenericViewSet):
    queryset = Game.objects.order_by('game_date')
    serializer_class = GameSerializer
    permission_classes = [permissions.AllowAny]
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = GameFilter
    pagination_class = StandardResultsSetPagination


class NextGamesView(mixins.ListModelMixin, GenericViewSet):
    serializer_class = NextGameSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        return GamesService.get_next_games()
