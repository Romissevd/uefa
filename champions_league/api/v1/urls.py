from rest_framework.routers import SimpleRouter

from champions_league.api.v1.views.game import GameViewSet, NextGamesView

router = SimpleRouter()
router.register('games', GameViewSet, basename='games')
router.register('next_games', NextGamesView, basename='next-games')

urlpatterns = []

urlpatterns += router.urls
