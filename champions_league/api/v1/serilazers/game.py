from rest_framework import serializers

from champions_league.models import Game
from team.models import Team


class TeamForGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ('id', 'name', 'short_name', 'logo')


class GameSerializer(serializers.ModelSerializer):
    home_team = TeamForGameSerializer()
    away_team = TeamForGameSerializer()

    class Meta:
        model = Game
        fields = (
            'id', 'home_team', 'away_team', 'game_date', 'goals_home', 'goals_away', 'goals_extra_home',
            'goals_extra_away', 'penalties_home', 'penalties_away', 'stage', 'group',
        )


class NextGameSerializer(serializers.ModelSerializer):
    home_team = TeamForGameSerializer()
    away_team = TeamForGameSerializer()

    class Meta:
        model = Game
        fields = (
            'id', 'home_team', 'away_team', 'game_date', 'stage', 'group',
        )
