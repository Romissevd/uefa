from datetime import date

from django_filters import rest_framework as filters

from champions_league.models import Game


class GameFilter(filters.FilterSet):
    game_date = filters.DateFilter()
    game_date_from = filters.DateFilter(field_name='game_date', lookup_expr='gte')
    game_date_to = filters.DateFilter(field_name='game_date', lookup_expr='lte')
    status = filters.ChoiceFilter(choices=Game.GameStatus.choices)
    start_season = filters.NumberFilter(max_value=date.today().year, min_value=2020, method='start_season_filter')

    @staticmethod
    def start_season_filter(queryset, name, value):
        return queryset.filter(start_season__year=value)
