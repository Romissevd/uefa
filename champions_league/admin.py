from django.contrib import admin

from champions_league.models import ChampionsLeagueStage


@admin.register(ChampionsLeagueStage)
class StageAdmin(admin.ModelAdmin):
    list_display = ('name',)
