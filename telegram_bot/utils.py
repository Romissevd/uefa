import functools

from django.utils import translation

from telegram_bot.services import get_user_from_telegram_action
from user_profile.models import UserProfile


def telegram_translatable(func):
    """
    Allows you to translate text inside a function. Translated to English by default
    The language is passed as a named parameter (language) to the decorated function.
    """
    @functools.wraps(func)
    def wrapper_localize(*args, **kwargs):
        telegram_action = args[0]
        user = get_user_from_telegram_action(telegram_action)
        user_language = getattr(getattr(user, 'profile', None), 'language', None)
        language = user_language or UserProfile.Language.EN
        kwargs['language'] = language
        with translation.override(language):
            value = func(*args, **kwargs)
        return value
    return wrapper_localize
