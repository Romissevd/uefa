from django.utils.translation import gettext as _

MATCHES = _('Matches')
BACK = _('Back')
LAST_MATCHES = _('Last matches')
NEXT_MATCHES = _('Next matches')
MENU = _('Menu')
BET = _('Bets')
MY_BET = _('My bets')
NEW_BET = _('New bet')
START_NEW_BET = _('Start bet')
ALL_BET = _('All bets')
TABLE = _('Table')
REGISTRATION = _('Registration')

START_MESSAGE_TO_USER = _('Hello, {}! Glad to see you again. What would you want?')
UNKNOWN_USER_MESSAGE = _("Sorry, I don't know you.")
GUEST = _('Guest')
MESSAGE_ABOUT_NEED_REGISTRATION = _('You need to register in the system to use this bot.')
GOODBYE_MESSAGE = _('Goodbye!')

CAN_SEE_MATCHES = _('In the meantime, you can see what the next matches will be like.')
APPLICATION_ON_MODERATION = _(
    'Your application for registration is pending. You will receive notification of the outcome of your application.',
)

NO_NEXT_MATCHES = _('Unfortunately, I have no information about the next matches.')
NO_LAST_MATCHES = _('Unfortunately, I have no information about the last matches.')

BETS_FORMAT_TEXT = _('Bets are accepted in the format X-X')
ALREADY_HAS_BETS = _('You have already placed all your bets. We are waiting for the results.')
MATCHES_WITHOUT_BETS = _("You haven't bet on these matches yet\n\n{}")
NO_BETS = _("You haven't placed bets on any of the next matches yet.")
