��          �   %   �      P     Q     Z     _  #   d     �  6   �     �  @   �          ,     4     9     A     I     V     c  	   |     �  <   �  <   �  F     (   M  7   v  3   �  o   �  B  R     �     �     �  #   �     �  6   �     	  @   "     c     p     x     }     �     �     �     �  	   �     �  <   �  <     F   J  (   �  7   �  3   �  o   &	                                        
                                           	                                          All bets Back Bets Bets are accepted in the format X-X Guest Hello, {}! Glad to see you again. What would you want? I do not understand you. In the meantime, you can see what the next matches will be like. Last matches Matches Menu My bets New bet Next matches Registration Sorry, I don't know you. Start bet Table Unfortunately, I have no information about the last matches. Unfortunately, I have no information about the next matches. You have already placed all your bets. We are waiting for the results. You haven't bet on these matches yet

{} You haven't placed bets on any of the next matches yet. You need to register in the system to use this bot. Your application for registration is pending. You will receive notification of the outcome of your application. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 All bets Back Bets Bets are accepted in the format X-X Guest Hello, {}! Glad to see you again. What would you want? I do not understand you. In the meantime, you can see what the next matches will be like. Last matches Matches Menu My bets New bet Next matches Registration Sorry, I don't know you. Start bet Table Unfortunately, I have no information about the last matches. Unfortunately, I have no information about the next matches. You have already placed all your bets. We are waiting for the results. You haven't bet on these matches yet

{} You haven't placed bets on any of the next matches yet. You need to register in the system to use this bot. Your application for registration is pending. You will receive notification of the outcome of your application. 