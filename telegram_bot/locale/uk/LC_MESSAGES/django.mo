��          �   %   �      `     a     j     o  #   t     �     �  6   �     �  @   �     8     E     M     R     Z     b     o     |  	   �     �  <   �  <   �  F     (   f  7   �  3   �  o   �     k     �  
   �     �  9   �     �  
     Y         i  _   �     �  
   	     	     	     0	     F	     b	  &   w	     �	     �	  `   �	  ^   
  `   z
  M   �
  U   )  �     �                                                     	                                 
                                          All bets Back Bets Bets are accepted in the format X-X Goodbye! Guest Hello, {}! Glad to see you again. What would you want? I do not understand you. In the meantime, you can see what the next matches will be like. Last matches Matches Menu My bets New bet Next matches Registration Sorry, I don't know you. Start bet Table Unfortunately, I have no information about the last matches. Unfortunately, I have no information about the next matches. You have already placed all your bets. We are waiting for the results. You haven't bet on these matches yet

{} You haven't placed bets on any of the next matches yet. You need to register in the system to use this bot. Your application for registration is pending. You will receive notification of the outcome of your application. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Усі ставки Назад Ставки Ставки приймаються у форматі X-X Прощавай! Гість Привіт, {}! Радий тебе знову бачити. Що би ти хотів? Я не розумію тебе. Поки ти можеш подивитися, які будуть наступні матчі. Попередні матчі Матчі Меню Мої ставки Нова ставка Наступні матчі Реєстрація Вибач, я тебе не знаю. Почати Таблиця На жаль, у мене немає інформації про попередні матчі. На жаль, у мене немає інформації про наступні матчі. Ти вже зробив всі свої ставки. Чекаємо на результати. Ти ще не зробив ставки на наступні матчі

{} Ти ще не зробив ставки на жоден наступний матч. Тобі потрібно зареєструватися в системі, щоб користуватися цим ботом. Твоя заявка на реєстрацію розглядається. Ти отримаєш повідомлення про результат розгляду твоєї заявки. 