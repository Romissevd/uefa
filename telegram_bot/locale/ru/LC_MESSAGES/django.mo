��          �   %   �      `     a     j     o  #   t     �     �  6   �     �  @   �     8     E     M     R     Z     b     o     |  	   �     �  <   �  <   �  F     (   f  7   �  3   �  o   �  �  k     8  
   L     W  9   d  	   �  
   �  V   �      
  e   +     �  
   �     �     �     �     �     	  '   $	     L	     h	  `   w	  b   �	  W   ;
  E   �
  Z   �
  �   4  �   �                                                 	                                 
                                          All bets Back Bets Bets are accepted in the format X-X Goodbye! Guest Hello, {}! Glad to see you again. What would you want? I do not understand you. In the meantime, you can see what the next matches will be like. Last matches Matches Menu My bets New bet Next matches Registration Sorry, I don't know you. Start bet Table Unfortunately, I have no information about the last matches. Unfortunately, I have no information about the next matches. You have already placed all your bets. We are waiting for the results. You haven't bet on these matches yet

{} You haven't placed bets on any of the next matches yet. You need to register in the system to use this bot. Your application for registration is pending. You will receive notification of the outcome of your application. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Все ставки Назад Ставки Ставки принимаются в формате X-X Пока! Гость Привет, {}! Рад вновь тебя видеть. Чтобы ты хотел? Я не понимаю тебя. Пока ты можешь посмотреть, какие будут следующие матчи. Последние матчи Матчи Меню Мои ставки Новая ставка Следующие матчи Регистрация Извини, я тебя не знаю Сделать ставку Таблица К соалению, у меня нет информации о последних матчах. К сожалению, у меня нет информации о следующих матчах. Ты уже сделал все свои ставки. Ждем результатов. Ты еще не сделал ставки на эти матчи

{} Ты еще не сделал ставки ни на один следующий матч. Тебе нужно зарегистрироваться в системе, чтобы пользоваться этим ботом. Твоя заявка на регистрацию находится на рассмотрении. Ты получишь уведомление о результате рассмотрения твоей заявки. 