import time

import requests
from django.core.management import BaseCommand

from telegram_bot import echo_bot


class Command(BaseCommand):
    help = 'Telegram bot'

    def handle(self, *args, **options):
        print('BOT IS RUNNING...')
        try:
            echo_bot.bot.infinity_polling(none_stop=True, timeout=60, interval=0)
        except requests.exceptions.ReadTimeout:
            time.sleep(15)
        print('BOT IS STOPPED...')
