from enum import Enum

import telebot
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from bet.models import Bet
from bet.service.bet_service import BetService
from champions_league.services.games import GamesService
from telegram_bot import buttons as btn
from telegram_bot import telegram_texts
from telegram_bot.services import get_user_from_telegram_action
from telegram_bot.tasks import registration_request_from_telegram
from telegram_bot.utils import telegram_translatable
from user_profile.models import TelegramRegistrationRequest

User = get_user_model()


bot = telebot.TeleBot(settings.TELEGRAM_TOKEN, parse_mode=None)


class CallBackData(Enum):
    REGISTRATION = 'registration'
    BACK_REGISTRATION = 'back_registration'
    NEXT_MATCHES = 'next_matches'
    BACK = 'back'
    BACK_TO_MENU = 'back_to_menu'
    BACK_TO_MENU_FROM_BET = 'back_to_menu_from_bet'
    MENU = 'menu'
    BET = 'bet'
    TABLE = 'table'
    USER_LAST_BET = 'user_last_bet'
    NEW_BET = 'new_bet'
    START_NEW_BET = 'start_new_bet'
    ALL_BET = 'all_bet'
    MATCHES = 'matches'
    LAST_MATCHES = 'last_matches'


@bot.message_handler(commands=['start'])
@telegram_translatable
def send_welcome(message, *args, **kwargs):
    telegram_user_name = message.from_user.username or message.from_user.id
    chat_id = message.chat.id
    telegram_users = User.objects.filter(
        profile__telegram_profiles__username=telegram_user_name,
        profile__telegram_profiles__isnull=False,
    )
    if telegram_users.exists():
        telegram_user = telegram_users.first()
        start_message = _(telegram_texts.START_MESSAGE_TO_USER).format(telegram_user.username)
        markup = telebot.types.InlineKeyboardMarkup()
        markup.row(btn.get_menu_btn(), btn.get_back_btn())
        bot.send_message(chat_id, start_message, reply_markup=markup)
    else:
        telegram_registration_requests = TelegramRegistrationRequest.objects.filter(
            telegram_username=telegram_user_name,
            status=TelegramRegistrationRequest.RequestStatus.IN_PROCESS,
        )
        if telegram_registration_requests.exists():
            bot.send_message(chat_id, _(telegram_texts.APPLICATION_ON_MODERATION))
            markup = telebot.types.InlineKeyboardMarkup()
            markup.row(btn.get_next_matches_btn(), btn.get_back_btn())
            bot.send_message(chat_id, _(telegram_texts.CAN_SEE_MATCHES), reply_markup=markup)
        else:
            guest_name = message.from_user.first_name or _(telegram_texts.GUEST)
            start_message = _(telegram_texts.MESSAGE_ABOUT_NEED_REGISTRATION).format(guest_name)
            markup = telebot.types.InlineKeyboardMarkup()
            reg_btn1 = telebot.types.InlineKeyboardButton(
                _(telegram_texts.REGISTRATION),
                callback_data=CallBackData.REGISTRATION.value,
            )
            reg_btn2 = telebot.types.InlineKeyboardButton(
                _(telegram_texts.BACK),
                callback_data=CallBackData.BACK_REGISTRATION.value,
            )
            markup.row(reg_btn1, reg_btn2)
            bot.send_message(chat_id, start_message, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.REGISTRATION.value)
@telegram_translatable
def registration(call, *args, **kwargs):
    chat_id = call.message.chat.id
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=telebot.types.InlineKeyboardMarkup())
    registration_request_from_telegram.s(call).apply_async()
    bot.send_message(chat_id, _(telegram_texts.APPLICATION_ON_MODERATION))
    markup = telebot.types.InlineKeyboardMarkup()
    markup.row(btn.get_next_matches_btn(), btn.get_back_btn())
    bot.send_message(chat_id, _(telegram_texts.CAN_SEE_MATCHES), reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.BACK_REGISTRATION.value)
@telegram_translatable
def back_from_registration(call, *args, **kwargs):
    chat_id = call.message.chat.id
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=telebot.types.InlineKeyboardMarkup())
    bot.send_message(chat_id, telegram_texts.GOODBYE_MESSAGE)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.BACK.value)
@telegram_translatable
def back_from_chat(call, *args, **kwargs):
    chat_id = call.message.chat.id
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=telebot.types.InlineKeyboardMarkup())
    bot.send_message(chat_id, _(telegram_texts.GOODBYE_MESSAGE))


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.MENU.value)
@telegram_translatable
def menu(call, *args, **kwargs):
    chat_id = call.message.chat.id
    markup = telebot.types.InlineKeyboardMarkup()
    markup.row(btn.get_matches_btn(), btn.get_bets_btn(), btn.get_table_btn(), btn.get_back_to_menu_btn())
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.BACK_TO_MENU.value)
@telegram_translatable
def back_to_menu(call, *args, **kwargs):
    chat_id = call.message.chat.id
    markup = telebot.types.InlineKeyboardMarkup()
    markup.row(btn.get_menu_btn(), btn.get_back_btn())
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.MATCHES.value)
@telegram_translatable
def matches_menu(call, *args, **kwargs):
    chat_id = call.message.chat.id
    markup = telebot.types.InlineKeyboardMarkup(row_width=1)
    markup.add(btn.get_next_matches_btn(), btn.get_last_matches_btn(), btn.get_back_to_menu_btn())
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.NEXT_MATCHES.value)
@telegram_translatable
def next_matches(call, *args, **kwargs):
    chat_id = call.message.chat.id
    matches = GamesService.get_next_games_text(kwargs['language'])
    if not matches:
        matches = _(telegram_texts.NO_NEXT_MATCHES)
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=telebot.types.InlineKeyboardMarkup())
    markup = telebot.types.InlineKeyboardMarkup()
    markup.row(btn.get_matches_btn(), btn.get_back_to_menu_btn())
    bot.send_message(chat_id, matches, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.LAST_MATCHES.value)
@telegram_translatable
def last_matches(call, *args, **kwargs):
    chat_id = call.message.chat.id
    matches = GamesService.get_list_last_games_text(kwargs['language'])
    if not matches:
        matches = _(telegram_texts.NO_LAST_MATCHES)
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=telebot.types.InlineKeyboardMarkup())
    markup = telebot.types.InlineKeyboardMarkup()
    markup.row(btn.get_matches_btn(), btn.get_back_to_menu_btn())
    bot.send_message(chat_id, matches, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.BET.value)
@telegram_translatable
def bet_menu(call, *args, **kwargs):
    chat_id = call.message.chat.id
    markup = telebot.types.InlineKeyboardMarkup(row_width=1)
    markup.add(btn.get_my_last_bet_btn(), btn.get_new_bet_btn(), btn.get_all_bet_btn(), btn.get_back_to_menu_btn())
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.NEW_BET.value)
@telegram_translatable
def new_bet(call, *args, **kwargs):
    chat_id = call.message.chat.id
    user = get_user_from_telegram_action(call.message)
    if not user:
        bot.send_message(
            chat_id,
            _(telegram_texts.UNKNOWN_USER_MESSAGE),
            reply_markup=telebot.types.InlineKeyboardMarkup(),
        )
        return
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=telebot.types.InlineKeyboardMarkup())
    text_message = BetService.get_next_games_without_bet_text(user, kwargs['language'])
    if text_message:
        text_message = _(telegram_texts.MATCHES_WITHOUT_BETS).format(text_message)
        markup = telebot.types.InlineKeyboardMarkup(row_width=1)
        markup.add(btn.get_start_new_bet_btn(), btn.get_bets_btn())
        bot.send_message(chat_id, text_message, reply_markup=markup)
    else:
        markup = telebot.types.InlineKeyboardMarkup(row_width=1)
        markup.add(btn.get_menu_btn(), btn.get_bets_btn())
        bot.send_message(chat_id, _(telegram_texts.ALREADY_HAS_BETS), reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.START_NEW_BET.value)
@telegram_translatable
def start_new_bet(call, *args, **kwargs):
    chat_id = call.message.chat.id
    bot.send_message(chat_id, _(telegram_texts.BETS_FORMAT_TEXT))
    message = call.message
    message.text = None
    create_new_bet(message)


@telegram_translatable
def create_new_bet(message, *args, **kwargs):
    result = message.text
    game_id = kwargs.get('game_id')
    chat_id = message.chat.id
    user = get_user_from_telegram_action(message)
    if not user:
        bot.send_message(
            chat_id,
            _(telegram_texts.UNKNOWN_USER_MESSAGE),
            reply_markup=telebot.types.InlineKeyboardMarkup(),
        )
        return
    if result and game_id:
        goals = []
        for goal in result.split('-'):
            if goal.isdigit():
                goals.append(int(goal))
            else:
                bot.send_message(chat_id, _(telegram_texts.BETS_FORMAT_TEXT))
                bot.register_next_step_handler(message, create_new_bet, game_id=game_id)
                return
        Bet.objects.create(user=user, game_id=game_id, goals_home=goals[0], goals_away=goals[1])
    games_without_bet = BetService.get_game_for_user_without_bet(user)
    if not games_without_bet.exists():
        markup = telebot.types.InlineKeyboardMarkup(row_width=1)
        markup.add(btn.get_menu_btn(), btn.get_bets_btn())
        bot.send_message(chat_id, _(telegram_texts.ALREADY_HAS_BETS), reply_markup=markup)
    else:
        game = games_without_bet.first()
        text_message = f'{game.home_team.name} - {game.away_team.name}'
        markup = telebot.types.InlineKeyboardMarkup(row_width=1)
        markup.add(btn.get_back_to_menu_btn())
        bot.send_message(chat_id, text_message, reply_markup=markup)
        bot.register_next_step_handler(message, create_new_bet, game_id=game.id)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.BACK_TO_MENU_FROM_BET.value)
@telegram_translatable
def back_to_menu_from_bet(call, *args, **kwargs):
    bot.clear_step_handler(call.message)
    back_to_menu(call)


@bot.callback_query_handler(func=lambda call: call.data == CallBackData.USER_LAST_BET.value)
@telegram_translatable
def user_last_bet(call, *args, **kwargs):
    chat_id = call.message.chat.id
    user = get_user_from_telegram_action(call.message)
    if not user:
        bot.send_message(
            chat_id,
            telegram_texts.UNKNOWN_USER_MESSAGE,
            reply_markup=telebot.types.InlineKeyboardMarkup(),
        )
        return
    user_text_bet = BetService.get_user_last_bets_text(user, kwargs['language'])
    if not user_text_bet:
        user_text_bet = _(telegram_texts.NO_BETS)
    bot.edit_message_reply_markup(chat_id, call.message.id, reply_markup=telebot.types.InlineKeyboardMarkup())
    markup = telebot.types.InlineKeyboardMarkup(row_width=1)
    markup.add(btn.get_menu_btn(), btn.get_bets_btn())
    bot.send_message(chat_id, user_text_bet, reply_markup=markup)


@bot.message_handler(func=lambda m: True)
@telegram_translatable
def echo_all(message, *args, **kwargs):
    bot.reply_to(message, _('I do not understand you.'))
