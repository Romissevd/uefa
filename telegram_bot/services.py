from typing import Optional

from telebot import types as telebot_types

from core.models import User


def get_user_from_telegram_action(action: telebot_types) -> Optional[User]:
    if isinstance(action, telebot_types.CallbackQuery):
        chat = action.message.chat
    elif isinstance(action, telebot_types.Message):
        chat = action.chat
    else:
        return None
    try:
        return User.objects.get(profile__telegram_profiles__chat_id=chat.id)
    except User.DoesNotExist:
        return None
