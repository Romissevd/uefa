from telebot import types

from main import celery_app
from user_profile.models import TelegramProfile, TelegramRegistrationRequest


@celery_app.task(serializer='pickle')
def registration_request_from_telegram(telegram_call: types.CallbackQuery) -> None:
    first_name = telegram_call.from_user.first_name
    last_name = telegram_call.from_user.last_name
    username = telegram_call.from_user.username
    if not username:
        username = f'telegram_{telegram_call.from_user.id}'
    TelegramRegistrationRequest.objects.get_or_create(
        telegram_username=username,
        status=TelegramRegistrationRequest.RequestStatus.IN_PROCESS,
        defaults={
            'first_name': first_name,
            'last_name': last_name,
            'chat_id': telegram_call.message.chat.id,
            'user_id': telegram_call.from_user.id,
        },
    )


@celery_app.task(serializer='pickle')
def send_message_to_telegram_about_rejected_request(request: TelegramRegistrationRequest) -> None:
    from telegram_bot.echo_bot import bot
    username_str = request.get_name
    message = f'{username_str}, Вам отказано в регистрации. Причина - {request.comment}'
    bot.send_message(request.chat_id, message)


@celery_app.task(serializer='pickle')
def send_message_to_telegram_about_approved_request(telegram_profile: TelegramProfile) -> None:
    from telegram_bot.echo_bot import bot
    username_str = telegram_profile.user_profile.user.short_name
    message = f'{username_str}, Вы успешно зарегистрированы.'
    bot.send_message(telegram_profile.chat_id, message)
