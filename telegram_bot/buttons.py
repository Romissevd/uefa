import telebot
from django.utils.translation import gettext as _

from telegram_bot import echo_bot, telegram_texts


def get_matches_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.MATCHES),
        callback_data=echo_bot.CallBackData.MATCHES.value,
    )


def get_back_btn():
    return telebot.types.InlineKeyboardButton(_(telegram_texts.BACK), callback_data=echo_bot.CallBackData.BACK.value)


def get_back_to_menu_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.BACK),
        callback_data=echo_bot.CallBackData.BACK_TO_MENU.value,
    )


def get_last_matches_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.LAST_MATCHES),
        callback_data=echo_bot.CallBackData.LAST_MATCHES.value,
    )


def get_next_matches_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.NEXT_MATCHES),
        callback_data=echo_bot.CallBackData.NEXT_MATCHES.value,
    )


def get_menu_btn():
    return telebot.types.InlineKeyboardButton(_(telegram_texts.MENU), callback_data=echo_bot.CallBackData.MENU.value)


def get_bets_btn():
    return telebot.types.InlineKeyboardButton(_(telegram_texts.BET), callback_data=echo_bot.CallBackData.BET.value)


def get_my_last_bet_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.MY_BET),
        callback_data=echo_bot.CallBackData.USER_LAST_BET.value,
    )


def get_new_bet_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.NEW_BET),
        callback_data=echo_bot.CallBackData.NEW_BET.value,
    )


def get_start_new_bet_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.START_NEW_BET),
        callback_data=echo_bot.CallBackData.START_NEW_BET.value,
    )


def get_all_bet_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.ALL_BET),
        callback_data=echo_bot.CallBackData.ALL_BET.value,
    )


def get_table_btn():
    return telebot.types.InlineKeyboardButton(
        _(telegram_texts.TABLE),
        callback_data=echo_bot.CallBackData.TABLE.value,
    )
