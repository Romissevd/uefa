from datetime import timedelta

from django.db.models import QuerySet
from django.utils import timezone

from champions_league.models import Game
from champions_league.services.games import GamesService
from core.models import User
from user_profile.models import UserProfile


class BetService:

    @staticmethod
    def get_game_for_user_without_bet(user: User) -> QuerySet[Game]:
        max_bet_datetime = timezone.now() + timedelta(minutes=10)
        next_games = GamesService.get_next_games()
        return next_games.filter(game_date__gte=max_bet_datetime).exclude(bets__user=user).order_by('game_date')

    @classmethod
    def get_next_games_without_bet_text(
            cls, user: User,
            language: UserProfile.Language = UserProfile.Language.EN,
    ) -> str:
        games_without_bet = cls.get_game_for_user_without_bet(user)
        home_team_name = f'home_team__name_{language}'
        away_team_name = f'away_team__name_{language}'
        result = []
        for obj in games_without_bet.values(
                'home_team__name_en', 'away_team__name_en', home_team_name, away_team_name,
        ):
            home_team = obj[home_team_name] or obj['home_team__name_en']
            away_team = obj[away_team_name] or obj['away_team__name_en']
            result.append(f'{home_team} - {away_team}')
        return '\n'.join(result)

    @staticmethod
    def get_user_last_bets_text(
            user: User,
            language: UserProfile.Language = UserProfile.Language.EN,
    ) -> str:
        next_games = GamesService.get_next_games()
        home_team_name = f'home_team__name_{language}'
        away_team_name = f'away_team__name_{language}'
        result = []
        for obj in next_games.filter(bets__user=user).values(
                'home_team__name_en', 'away_team__name_en', 'bets__goals_home', 'bets__goals_away', home_team_name,
                away_team_name,
        ):
            home_team = obj[home_team_name] or obj['home_team__name_en']
            away_team = obj[away_team_name] or obj['away_team__name_en']
            result.append(f'{home_team} - {away_team}\n{obj["bets__goals_home"]} - {obj["bets__goals_away"]}')
        result = '\n'.join(result)
        return result

    @staticmethod
    def check_can_change_or_delete_bet(game: Game) -> bool:
        max_bet_datetime = game.game_date - timedelta(minutes=10)
        return max_bet_datetime > timezone.now()
