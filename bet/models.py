from django.db import models

from core.models import User
from mixins.model_mixin import DatetimeABC


class Bet(DatetimeABC):
    game = models.ForeignKey('champions_league.Game', on_delete=models.PROTECT, related_name='bets')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='bets')
    goals_home = models.SmallIntegerField()
    goals_away = models.SmallIntegerField()

    class Meta:
        unique_together = (
            ('game', 'user')
        )
