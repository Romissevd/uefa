from rest_framework import routers

from bet.api.v1.views.user_bets import UserBetsViewSet

router = routers.SimpleRouter()
router.register('user_bets', UserBetsViewSet, basename='user-bets')


urlpatterns = []

urlpatterns += router.urls
