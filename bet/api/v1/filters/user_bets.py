from django.db.models import Q
from django_filters import rest_framework as filters


class UserBetsFilter(filters.FilterSet):
    team = filters.CharFilter(method='team_filter')
    game_status = filters.ChoiceFilter(field_name='game__status')
    game_date = filters.DateFilter(field_name='game__game_date__date')
    game_date_from = filters.DateFilter(field_name='game__game_date__date', lookup_expr='gte')
    game_date_to = filters.DateFilter(field_name='game__game_date__date', lookup_expr='lte')
    stage = filters.CharFilter(method='stage_game_filter')
    group = filters.CharFilter(field_name='game__group', lookup_expr='iexact')

    @staticmethod
    def team_filter(queryset, name, value):
        return queryset.filter(
            Q(game__home_team__name__icontains=value) |
            Q(game__away_team__name__icontains=value),
        )

    @staticmethod
    def stage_game_filter(queryset, name, value):
        stage_ids = [stage_id for stage_id in value.split(',') if stage_id.isdigit()]
        return queryset.filter(game__stage_id__in=stage_ids)
