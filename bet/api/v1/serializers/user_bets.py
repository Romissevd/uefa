from rest_framework import exceptions, serializers

from bet.models import Bet
from bet.service.bet_service import BetService
from champions_league.api.v1.serilazers.game import GameSerializer


class UserBetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bet
        fields = ('id', 'goals_home', 'goals_away', 'game')

    def validate(self, attrs):
        instance = self.instance
        game = attrs.get('game')
        user = getattr(self.context.get('request'), 'user', None)
        if instance and not BetService.check_can_change_or_delete_bet(instance.game):
            raise exceptions.ValidationError('You cannot update this bet')
        if not instance and game and game not in BetService.get_game_for_user_without_bet(user):
            raise exceptions.ValidationError('You cannot create this bet')
        return super().validate(attrs)

    def update(self, instance, validated_data):
        validated_data.pop('game', None)
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['game'] = GameSerializer(instance=instance.game).data
        return response
