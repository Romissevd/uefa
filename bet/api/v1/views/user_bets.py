from django_filters import rest_framework as filters
from rest_framework import exceptions, permissions, viewsets

from bet.api.v1.filters.user_bets import UserBetsFilter
from bet.api.v1.serializers.user_bets import UserBetSerializer
from bet.models import Bet
from bet.service.bet_service import BetService
from mixins.pagination_mixin import StandardResultsSetPagination
from mixins.permission_mixin import IsFanUserPermission


class UserBetsViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsFanUserPermission)
    queryset = Bet.objects.all()
    serializer_class = UserBetSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = UserBetsFilter
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_destroy(self, instance):
        if not BetService.check_can_change_or_delete_bet(instance.game):
            raise exceptions.ValidationError('You cannot delete this bet')
        instance.delete()
