FROM python:3.12-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV HOME=/usr/src
ENV APP_HOME=/usr/src/app
ENV MEDIA_ROOT=/mnt/uefa

RUN mkdir -p $APP_HOME $MEDIA_ROOT/static $MEDIA_ROOT/media

WORKDIR $APP_HOME

RUN apt-get update && \
    apt-get install -y vim curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY requirements.txt .

RUN python -m pip install --upgrade pip &&  \
    pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app
