import random


class CoreService:

    @staticmethod
    def password_generator(len_password: int = 8, with_spec_symbols: bool = True) -> str:
        chars = 'abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        if with_spec_symbols:
            chars += '+-/*!&$#?=@<>'
        password = ''.join([random.choice(chars) for _ in range(len_password)])
        return password
