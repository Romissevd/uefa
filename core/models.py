from django.contrib.auth.models import AbstractUser
from django.db import models

from core.manager import CustomUserManager


class User(AbstractUser):
    class TypeUser(models.TextChoices):
        FAN = 'fan'
        MANAGER = 'manager'

    email = None
    type_user = models.CharField(max_length=30, choices=TypeUser.choices, default=TypeUser.FAN)
    middle_name = models.CharField(max_length=50, default='')

    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    @property
    def short_name(self):
        return f'{self.last_name} {self.first_name}'
