from django.db import models


class NameABC(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        abstract = True


class DatetimeABC(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ContactABC(models.Model):
    class TypeContact(models.TextChoices):
        PHONE = 'phone'
        EMAIL = 'email'

    value = models.CharField(max_length=100)
    type_contact = models.CharField(max_length=20, choices=TypeContact.choices, default=TypeContact.PHONE)

    class Meta:
        abstract = True
