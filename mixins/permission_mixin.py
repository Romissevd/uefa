from rest_framework import permissions

from core.models import User


class IsManagerUserPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.type_user == User.TypeUser.MANAGER


class IsFanUserPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.type_user == User.TypeUser.FAN
